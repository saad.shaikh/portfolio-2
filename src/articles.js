const articles = [
    {
        id: 3,
        title: "How to style a simple hamburger menu",
        published_on: "06/06/2022",
        link: "https://medium.com/@saad.shaikh./how-to-style-a-simple-hamburger-menu-112bcead41a6",
        brief: "Do you want to style a simple hamburger menu with as little code as possible? Well, I’ve got you! All you need is one empty div element and some CSS. First, let’s decide what the hamburger menu will look like. I want it to be 20px tall and 20px wide…"
    },
    {
        id: 2,
        title: "How to create a portfolio website using Gatsby",
        published_on: "02/11/2021",
        link: "https://medium.com/@saad.shaikh./how-to-create-a-portfolio-website-using-gatsby-7f2c93b86e2b",
        brief: "In this article I’m going to show you how to use Gatsby to create a personal portfolio website. This website will include a home page, a projects page and an blog articles page. First, let’s set up the structure of the website. …"
    },
    {
        id: 1,
        title: "How to create an API that extracts data from websites",
        published_on: "21/10/2021",
        link: "https://medium.com/@saad.shaikh./how-to-create-an-api-that-extracts-data-from-websites-37f9c6d9c3bc",
        brief: "Hi, I a Frontend Developer and I am going to show you how to create an API that aggregates data from various websites. I will be using Node JS with the extensions axios, cheerio and express as well as nodemon for the development phase. So here goes.. First…"
    }
];

let num = 0;
articles.map(article => {
    document.getElementById("articles").innerHTML +=
        `
        <div id="${"article-" + num}" class="article">
            <h3 id="${"article-title-" + num}">${article.title}</h3>
            <p>${article.published_on}</p>
            <p>${article.brief}</p>
            <a href="${article.link}" target="_blank">Read more →</a>
        </div>
        `;
});

document.getElementById("search").addEventListener("input", e => {
    console.log(e.target.value);
    for (let i = 0; i < document.getElementById("articles").childElementCount; i++) {
        if (document.getElementById("articles").children[i].innerText.toLowerCase().includes(e.target.value.trim().toLowerCase())) {
            document.getElementById("articles").children[i].style.display = "";
        } else {
            document.getElementById("articles").children[i].style.display = "none";
        }
    }
});