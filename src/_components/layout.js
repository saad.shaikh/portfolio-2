document.getElementById("header").innerHTML =
    `
    <h1>Saad Shaikh</h1></a>
    <nav>
        <a id="to_home" href="/#">Home</a>
        <a id="to_projects" href="/projects#">Projects</a>
        <a id="to_articles" href="/articles#">Articles</a>
        <a href="#contact">Contact</a>
    </nav>
    `;

document.getElementById("footer").innerHTML =
    `
    <p id="contact">You may contact me at <a href="mailto:saad.shaikh.dev@protonmail.com">saad.shaikh.dev@protonmail.com</a>.</p>
    <div class="gallery">
        <a href="https://gitlab.com/saad.shaikh/" target="_blank" rel="noreferrer">
            <img width="160" height="45" src="https://about.gitlab.com/images/press/logo/png/gitlab-logo-100.png" width="150" alt="Gitlab" loading="lazy">
        </a>
        <a href="https://www.linkedin.com/in/saad-shaikh-dev/" target="_blank" rel="noreferrer">
            <img width="160" height="45" src="https://upload.wikimedia.org/wikipedia/commons/0/01/LinkedIn_Logo.svg" width="150" alt="LinkedIn" loading="lazy">
        </a>
        <a href="https://medium.com/@saad.shaikh" target="_blank" rel="noreferrer">
            <img width="160" height="45" src="https://upload.wikimedia.org/wikipedia/commons/0/0d/Medium_%28website%29_logo.svg" width="150" alt="Medium" loading="lazy">
        </a>
        <a href="https://stackoverflow.com/users/14394673/saad-shaikh" target="_blank" rel="noreferrer">
            <img width="160" height="45" src="https://upload.wikimedia.org/wikipedia/commons/0/02/Stack_Overflow_logo.svg" width="150" alt="Stack Overflow" loading="lazy">
        </a>
    </div>
    <p>Copyright 2022 - Saad Shaikh</p>
    `;