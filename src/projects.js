import { anchor_a, anchor_b } from "./anchors.js";

try {
    let response = await fetch(
        "https://gitlab.com/api/v4/projects?owned=true&visibility=public&per_page=100&search=", {
        method: 'GET',
        headers: {
            "Access-Control-Request-Headers": anchor_a(),
            "Authorization": anchor_b()
        },
        redirect: 'follow'
    });
    let result = await response.json();
    sessionStorage.setItem("projects", JSON.stringify(result));
} catch (err) { console.error(err); }

const get_projects = async () => {
    document.getElementById("projects").innerHTML = "";
    let projects = JSON.parse(sessionStorage.getItem("projects"));
    projects.map(project => {
        let date = (date) => date.substr(8, 2) + "-" + date.substr(5, 2) + "-" + date.substr(0, 4);
        document.getElementById("projects").innerHTML +=
            `
                <div class="project">
                    <img width="300" height="215" src="${project.avatar_url ? project.avatar_url : "https://upload.wikimedia.org/wikipedia/commons/e/e1/GitLab_logo.svg"}" alt="${project.name}" loading="lazy">
                    <h3>${project.name}</h3>
                    <p><strong>Created on:</strong>&emsp; &emsp; &emsp; &emsp;${date(project.created_at)}
                    </p>
                    <p><strong>Last modified on:</strong> ${date(project.last_activity_at)}</p>
                    <hr>
                    <p class="desc">${project.description && project.description.replace("\n", `<br>`)}</p>
                    <hr>
                    <a href="${project.web_url}" target="_blank">Go to repository →</a>
                </div >
    `;
    });
}; get_projects();

document.getElementById("search").addEventListener("input", e => {
    console.log(e.target.value);
    for (let i = 0; i < document.getElementById("projects").childElementCount; i++) {
        if (document.getElementById("projects").children[i].innerText.toLowerCase().includes(e.target.value.trim().toLowerCase())) {
            document.getElementById("projects").children[i].style.display = "";
        } else {
            document.getElementById("projects").children[i].style.display = "none";
        }
    }
});