let stacks = [
    {
        name: "Svelte",
        img: "https://upload.wikimedia.org/wikipedia/commons/1/1b/Svelte_Logo.svg"
    },
    {
        name: "Gatsby.js",
        img: "https://upload.wikimedia.org/wikipedia/en/thumb/d/d0/Gatsby_Logo.png/220px-Gatsby_Logo.png"
    },
    {
        name: "Next.js",
        img: "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8e/Nextjs-logo.svg/207px-Nextjs-logo.svg.png?20190307203525"
    },
    {
        name: "Vue.js",
        img: "https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Vue.js_Logo_2.svg/512px-Vue.js_Logo_2.svg.png?20170919082558"
    },
    {
        name: "Vite",
        img: "https://vitejs.dev/logo.svg"
    },
    {
        name: "Rust",
        img: "./_media/logos/Rust.svg"
    }
];
for (let i = stacks.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    const temp = stacks[i];
    stacks[i] = stacks[j];
    stacks[j] = temp;
}
for (let stack_num = 0; stack_num < stacks.length; stack_num++) {
    document.getElementById("stack-" + stack_num) &&
        document.getElementById("stack-" + stack_num).setAttribute("alt", stacks[stack_num].name);
    document.getElementById("stack-" + stack_num) &&
        document.getElementById("stack-" + stack_num).setAttribute("src", stacks[stack_num].img);
}