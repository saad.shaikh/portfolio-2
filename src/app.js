if (localStorage.getItem("visits") === null) localStorage.setItem("visits", 0);
else {
    var visits = parseInt(localStorage.getItem("visits"));
    window.onload = localStorage.setItem("visits", ++visits);
}
let user_data = {
    location: Intl.DateTimeFormat().resolvedOptions().timeZone.split("/")[1],
    visits: localStorage.getItem("visits"),
};
console.log(`A user from ${user_data.location} visited your site ${user_data.visits} times`);

/*const routes = [
    { id: "to_home", link: "/#" },
    { id: "to_projects", link: "/projects#" },
    { id: "to_articles", link: "/articles#" }
];
for (let i = 0; i < routes.length; i++) {
    document.getElementById(routes[i].id).addEventListener("click", e => {
        e.preventDefault();
        history.pushState({}, "", routes[i].link);
    });
}*/