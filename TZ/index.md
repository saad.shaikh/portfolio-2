{str,str}*6 stacks = [
    {
        name: "Svelte",
        img: "https://upload.wikimedia.org/wikipedia/commons/1/1b/Svelte_Logo.svg"
    },
    {
        name: "Gatsby.js",
        img: "https://upload.wikimedia.org/wikipedia/en/thumb/d/d0/Gatsby_Logo.png/220px-Gatsby_Logo.png"
    },
    {
        name: "Next.js",
        img: "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8e/Nextjs-logo.svg/207px-Nextjs-logo.svg.png?20190307203525"
    },
    {
        name: "Vue.js",
        img: "https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Vue.js_Logo_2.svg/512px-Vue.js_Logo_2.svg.png?20170919082558"
    },
    {
        name: "Vite",
        img: "https://vitejs.dev/logo.svg"
    },
    {
        name: "Rust",
        img: "https://upload.wikimedia.org/wikipedia/commons/d/d5/Rust_programming_language_black_logo.svg"
    }
];
{str,str}*6 shuffled_stacks = shuffled stacks

iter [0 .. length shuffled_stacks - 1] stack_num
    if document id ("stack-" + stack_num) exists then document id ("stack-" + stack_num) attr "alt" = stacks[stack_num] name
    if document id ("stack-" + stack_num) exists then document id ("stack-" + stack_num) attr "src" = stacks[stack_num] img